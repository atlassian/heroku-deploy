import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from http import HTTPStatus
from unittest import TestCase

import pytest

from pipe.main import Heroku, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


HEROKU_SOURCE_URL = 'https://api.heroku.com/sources'
HEROKU_SOURCE_DATA_URL = "https://api.heroku.com/sources/1234.tgz"
HEROKU_OUTPUT_STREAM_URL = "https://build-output.heroku.com/streams/01234567-89ab-cdef-0123-456789abcdef"
HEROKU_BUILD_URL = "https://api.heroku.com/apps/test-app-name/builds"
HEROKU_BUILD_ID = "01234567-89ab-cdef-0123-456789abcdef"


class HerokuDeployTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, requests_mock):
        self.caplog = caplog
        self.mocker = mocker
        self.request_mock = requests_mock

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_fail_wrong_api_key(self):
        self.mocker.patch.dict(
            os.environ, {
                "HEROKU_API_KEY": "test-wrong-api-key",
                "HEROKU_APP_NAME": "test-wrong-app-name",
                "ZIP_FILE": "package.tar.gz",
            }
        )

        pipe = Heroku(
            schema=schema, check_for_newer_version=True)

        self.request_mock.register_uri(
            'POST', HEROKU_SOURCE_URL,
            text='',
            status_code=HTTPStatus.NOT_FOUND
        )

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(
            out.getvalue(),
            '✖ Failed to create sources'
        )

    def test_success_app_deployed_successfully_default(self):
        self.mocker.patch.dict(
            os.environ, {
                "HEROKU_API_KEY": "test-api-key",
                "HEROKU_APP_NAME": "test-app-name",
                "ZIP_FILE": "package.tar.gz",
                "BITBUCKET_COMMIT": "abc"
            }
        )
        pipe = Heroku(schema=schema, check_for_newer_version=True)

        source_data = {
            "source_blob": {
                "get_url": HEROKU_SOURCE_DATA_URL,
                "put_url": HEROKU_SOURCE_DATA_URL
            }
        }
        self.request_mock.register_uri(
            'POST', HEROKU_SOURCE_URL,
            json=source_data,
            status_code=HTTPStatus.CREATED
        )

        self.mocker.patch.object(Heroku, 'read_file')

        self.request_mock.register_uri(
            'PUT', HEROKU_SOURCE_DATA_URL,
            status_code=HTTPStatus.OK,
            text='ok'
        )

        build_data = {
            'id': HEROKU_BUILD_ID,
            'output_stream_url': HEROKU_OUTPUT_STREAM_URL
        }
        self.request_mock.register_uri(
            'POST', HEROKU_BUILD_URL,
            status_code=HTTPStatus.CREATED,
            json=build_data
        )

        self.request_mock.register_uri(
            'GET', HEROKU_OUTPUT_STREAM_URL,
            status_code=HTTPStatus.OK,
            json=build_data
        )

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                pipe.run()

        self.assertRegex(out.getvalue(), 'Successfully started a new build')
        self.assertEqual(pytest_wrapped_e.type, SystemExit)

    def test_success_app_deployed_wait(self):
        self.mocker.patch.dict(
            os.environ, {
                "HEROKU_API_KEY": "test-api-key",
                "HEROKU_APP_NAME": "test-app-name",
                "ZIP_FILE": "package.tar.gz",
                "BITBUCKET_COMMIT": "abc",
                "WAIT": "True"
            }
        )
        pipe = Heroku(schema=schema, check_for_newer_version=True)

        source_data = {
            "source_blob": {
                "get_url": HEROKU_SOURCE_DATA_URL,
                "put_url": HEROKU_SOURCE_DATA_URL
            }
        }
        self.request_mock.register_uri(
            'POST', HEROKU_SOURCE_URL,
            json=source_data,
            status_code=HTTPStatus.CREATED
        )

        self.mocker.patch.object(Heroku, 'read_file')

        self.request_mock.register_uri(
            'PUT', HEROKU_SOURCE_DATA_URL,
            status_code=HTTPStatus.OK,
            text='ok'
        )

        build_data = {
            'id': HEROKU_BUILD_ID,
            'output_stream_url': HEROKU_OUTPUT_STREAM_URL
        }
        self.request_mock.register_uri(
            'POST', HEROKU_BUILD_URL,
            status_code=HTTPStatus.CREATED,
            json=build_data
        )

        self.request_mock.register_uri(
            'GET', HEROKU_OUTPUT_STREAM_URL,
            status_code=HTTPStatus.OK,
            json=build_data
        )

        deployment_data = {
            'status': 'succeeded'
        }

        self.request_mock.register_uri(
            'GET', f"{HEROKU_BUILD_URL}/{HEROKU_BUILD_ID}",
            status_code=HTTPStatus.OK,
            json=deployment_data
        )

        with capture_output() as out:
            pipe.run()

        self.assertRegex(out.getvalue(), 'Successfully deployed the application to Heroku')

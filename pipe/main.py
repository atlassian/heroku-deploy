import os
import requests
from colorama import Fore
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger, get_variable


COMMON_HEADERS = {"Accept": "application/vnd.heroku+json; version=3"}
HEROKU_URL = 'https://api.heroku.com'


class HTTPBearerAuth(requests.auth.AuthBase):
    """HTTP Bearer authentication for requests.

    Borrowed from https://github.com/kennethreitz/requests/issues/4437
    """

    def __init__(self, token):
        self.token = token

    def __eq__(self, other):
        return self.token == getattr(other, 'token', None)

    def __ne__(self, other):
        return not self == other

    def __call__(self, r):
        r.headers['Authorization'] = 'Bearer ' + self.token
        return r


def cyan(message):
    return f'{Fore.CYAN}{message}{Fore.RESET}'


schema = {
    "HEROKU_API_KEY": {"required": True, "type": "string"},
    "HEROKU_APP_NAME": {"required": True, "type": "string"},
    "ZIP_FILE": {"required": False, "type": "string"},
    "WAIT": {"required": False, "type": "boolean", "default": False},
    "CONFIG_VARS": {"required": False, "type": "dict"},
    "ACTION": {"required": False, "type": "string", "default": "deploy"},
    "DEBUG": {"required": False, "type": "boolean", "default": False}
}


logger = get_logger()


class Heroku(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        self.action = self.get_action()
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.bearer_auth = HTTPBearerAuth(token=self.get_variable('HEROKU_API_KEY'))
        self.app_name = self.get_variable('HEROKU_APP_NAME')

    @staticmethod
    def get_action():
        action = os.getenv('ACTION', 'deploy')
        if action == 'update':
            schema['CONFIG_VARS']['required'] = True
        else:
            schema['ZIP_FILE']['required'] = True

        return action

    def run(self):
        if self.action == 'update':
            self.update()
        else:
            self.deploy()

    def update(self):
        config_vars_data = self.get_variable('CONFIG_VARS')

        logger.info('Updating config vars on Heroku...')

        headers = {'Content-Type': 'application/json'}
        headers.update(COMMON_HEADERS)

        response = requests.patch(
            f'https://api.heroku.com/apps/{self.app_name}/config-vars',
            headers=COMMON_HEADERS,
            auth=self.bearer_auth,
            json=config_vars_data
        )

        if response.status_code != 200:
            logger.error(
                f'Failed to update config vars. Expected HTTP Status 200, got {response.status_code}. Error message: {response.text}'
            )

        logger.info(f'Successfully updated config vars. \n\t'
                    f'Application dashboard: https://dashboard.heroku.com/apps/{self.app_name}\n\t')

    def deploy(self):
        zip_file = self.get_variable('ZIP_FILE')
        wait = self.get_variable('WAIT')

        logger.info('Creating Heroku sources...')

        response = requests.post('https://api.heroku.com/sources',
                                 headers=COMMON_HEADERS,
                                 auth=self.bearer_auth)

        if response.status_code != 201:
            self.fail(
                f'Failed to create sources. Expected HTTP Status 201, got {response.status_code}. Error message: {response.text}'
            )

        response_data = response.json()
        put_url = response_data['source_blob']['put_url']
        get_url = response_data['source_blob']['get_url']

        binary_data = self.read_file(zip_file)

        logger.info(f'Uploading binary sources from {zip_file}')

        response = requests.put(put_url, data=binary_data)

        if response.status_code != 200:
            self.fail(
                f'Failed to upload binary sources. Expected HTTP Status 200, got {response.status_code}. Error message: {response.text}'
            )

        headers = {'Content-Type': 'application/json'}
        headers.update(COMMON_HEADERS)
        data = {
            'source_blob': {
                'url': get_url,
                'version': get_variable('BITBUCKET_COMMIT')
            }
        }

        logger.info('Starting a new build in Heroku...')

        response = requests.post(f'https://api.heroku.com/apps/{self.app_name}/builds',
                                 headers=headers,
                                 auth=self.bearer_auth,
                                 json=data)

        if response.status_code != 201:
            self.fail(
                f'Failed to upload binary sources. Expected HTTP Status 201, got {response.status_code}. Error message: {response.text}'
            )

        data = response.json()
        output_stream_url = data['output_stream_url']
        deployment_id = data['id']

        streaming_response = requests.get(output_stream_url, stream=True)

        build_logs_url = f'https://dashboard.heroku.com/apps/{self.app_name}/activity/builds/{deployment_id}'

        logger.info(
            f'Started new build for the application. Build logs URL: {build_logs_url}')

        self.success(f'Successfully started a new build id:{deployment_id}. Follow the build logs here: {build_logs_url}',
                     do_exit=not wait)

        try:
            print(cyan('>>>>BEGIN streaming output from heroku>>>'), flush=True)
            for line in streaming_response.iter_lines():
                print(cyan(line.decode()), flush=True)
        finally:
            print(cyan('<<<<<END streaming output from heroku'), flush=True)

        logger.info(f'Getting the build info for deployment: {deployment_id}')

        response = requests.get(f'https://api.heroku.com/apps/{self.app_name}/builds/{deployment_id}',
                                headers=COMMON_HEADERS,
                                auth=self.bearer_auth)

        if response.status_code != 200:
            self.fail(
                f'Failed to get the build data. Expected HTTP Status 200, got {response.status_code}. Error message: {response.text}'
            )

        deployment_result_data = response.json()

        if deployment_result_data['status'] == 'failed':
            self.fail(
                f'Heroku build failed! Check the logs for errors: {build_logs_url}'
            )

        self.success(f'Successfully deployed the application to Heroku. \n\t'
                     f'Application dashboard: https://dashboard.heroku.com/apps/{self.app_name}\n\t'
                     f'Build logs: {build_logs_url}')

    def read_file(self, filename, mode='rb'):
        if not os.path.exists(filename):
            self.fail(f"File {filename} doesn't exist.")

        with open(filename, mode) as zipfile:
            return zipfile.read()


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = Heroku(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()

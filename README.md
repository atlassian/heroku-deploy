# Bitbucket Pipelines Pipe: Heroku deploy

Deploy your application to Heroku

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/heroku-deploy:2.4.1
  variables:
    HEROKU_API_KEY: '<string>'
    HEROKU_APP_NAME: '<string>'
    # ACTION: '<string>' # Optional. 'update' or 'deploy' # Default 'deploy'.
    # ZIP_FILE: '<string>' # Optional. 'ACTION' 'deploy' specific.
    # WAIT: '<boolean>' # Optional. 'ACTION' 'deploy' specific # Default: 'false'.
    # CONFIG_VARS: '<json>' # Optional. 'ACTION' 'update' specific.
    # DEBUG: '<boolean>' # Optional. # Default: 'false'.
```
## Variables

| Variable            | Usage                                                                                                                                                         |
|---------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| HEROKU_APP_NAME (*) | Your application name.                                                                                                                                        |
| HEROKU_API_KEY (*)  | Heroku API key.                                                                                                                                               |
| ACTION              | The operation to perform. Valid options are `update` or `deploy`. Default: `deploy`.                                                                          |
| ZIP_FILE            | Name of the zip file containing your sources. Required if `ACTION` is set to `deploy`.                                                                        |
| WAIT                | Wait until the Heroku deployment completes. `ACTION` `deploy` specific. Default: `false`.                                                                     |
| CONFIG_VARS         | JSON document containing heroku configuration variables. The value should be a dictionary(mapping) {"key": "value"}. Required if `ACTION` is set to `update`. |
| DEBUG               | Turn on extra debug information. Default: `false`.                                                                                                            |


_(*) = required variable._

## Prerequisites

To start using this pipe you need to have a [Heroku account](https://dashboard.heroku.com/apps). In addition to that you have to provide
an API key. You can get this either from your [account settings page](https://dashboard.heroku.com/account) or by using the Heroku CLI util:

```bash
$ heroku auth:token
```

The pipe will deploy your application using Heroku Platfrom API and will authenticate requests using the provided token.
Read the official [Heroku documentation](https://devcenter.heroku.com/articles/authentication) for more details.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/heroku-deploy:2.4.1
    variables:
      HEROKU_API_KEY: $HEROKU_API_KEY
      HEROKU_APP_NAME: 'My awesome shiny app'
      ZIP_FILE: 'your-app-sources.tar.gz'
```

Advanced example:

You can set the `WAIT` parameter to `true` to tell the pipe to wait for the Heroku build completion:

```yaml
script:
  - pipe: atlassian/heroku-deploy:2.4.1
    variables:
      HEROKU_API_KEY: $HEROKU_API_KEY
      HEROKU_APP_NAME: 'My awesome shiny app'
      ZIP_FILE: 'your-app-sources.tar.gz'
      WAIT: 'true' # wait for build completion and exit the pipe
```

Update configuration variables for your application on heroku:

```yaml
script:
  - pipe: atlassian/heroku-deploy:2.4.1
    variables:
      HEROKU_API_KEY: $HEROKU_API_KEY
      HEROKU_APP_NAME: 'My awesome shiny app'
      ACTION: "update"
      CONFIG_VARS:  >
        {
           "test1": $TEST1,
           "test2": "test2"
        }
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,heroku

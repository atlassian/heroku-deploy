# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.4.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.2.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit to fix vulnerability with certify CVE-2023-37920.

## 2.1.0

- minor: Internal maintenance: Bump version of the base Docker image to python:3.10-slim.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.

## 2.0.0

- major: Implement configuration vars update for application on heroku.
- patch: Internal maintenance: update community questions link.
- patch: Internal maintenance: update dependencies in requirements.
- patch: Internal maintenance: update git submodule.
- patch: Internal maintenance: update versions of pipes atlassian/default-image, bitbucket-pipe-release.

## 1.2.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.3

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.2

- patch: Update the Readme with a new Atlassian Community link.

## 1.1.1

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.1.0

- minor: Added a warning message when new version of the pipe is available

## 1.0.4

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 1.0.3

- patch: Fix the bug with wait parameter being ignored

## 1.0.2

- patch: Added code style checks

## 1.0.1

- patch: Internal maintenance: update pipes toolkit version.

## 1.0.0

- major: Changed variable WAIT to false by default to reduce the time of pipe execution

## 0.1.2

- patch: Minor documentation updates

## 0.1.1

- patch: Fixed WAIT parameter type conversion

## 0.1.0

- minor: Initial release
- patch: Fixed the build script
